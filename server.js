const express = require('express');
const bodyParser = require('body-parser');
const Cosmic = require('cosmicjs')();
const app = express();
var cors = require('cors');
var userLocaliztion ="he-IL";//"en-US"

const sesClient = require('./ses-client');

app.use(bodyParser.urlencoded({ extended: false }));  
app.use(bodyParser.json());
app.use(cors());

app.get('/', (req, res) => {
})

app.get('/sendEmails', (req, res) => {
    var queryString = req.url;
    try {
        queryString = decodeURI(queryString);
        let input = req.params["name"]
        queryString = queryString.split('/??')[1];
        var arr = queryString.split('&');
        let name = arr[0].split('=')[1]
        let phone = arr[1].split('=')[1]
        let how = arr[2].split('=')[1]
        console.log(arr);
        let emailTitle = "  היי דור!  "+name+"  רוצה לברר על אימונים  ";
        let emailBody = " שם:  "+name +" טלפון "+phone+" שמע עלייך דרך:   "+how
        sesClient.sendEmail('benforcapita@gmail.com',emailTitle ,emailBody );

      } catch(e) { 
        console.error(e);
      }
   
   // res.send('Email is sent!');
});

app.get('/images', (req, res) => {
const bucket = Cosmic.bucket({
  slug: 'dorswebsite'
})
const data = bucket.getMedia()

bucket.getMedia({
  }).then(data => {
    console.log(data)
    res.json(data);
  }).catch(err => {
    console.log(err)
  })
});
app.get('/aboutMe',(req,res)=>
{
    userLocaliztion = "he-IL"//:"en-US"
const bucket = Cosmic.bucket({
  slug: 'dorswebsite'
})
const data = bucket.getObject({
    slug: 'aboutme',
    locale:userLocaliztion
  }).then(data => {
    res.json(data);
  });
}) 

app.get('/title',(req,res)=>
{
const bucket = Cosmic.bucket({
  slug: 'dorswebsite'
})
const data = bucket.getObject({
    slug: 'title',
    locale:userLocaliztion
  }).then(data => {
    res.json(data["object"]["content"]);
  });
}) 

app.get('/contectMeData',(req,res)=>
{
const bucket = Cosmic.bucket({
  slug: 'dorswebsite'
})
const data = bucket.getObject({
    slug: 'dictionery',
    locale:userLocaliztion
  }).then(data => {
    console.log(data["object"]["metadata"]["dictionary"]);
    res.json(data["object"]["metadata"]["dictionary"]);
  });
}) 
app.listen(3001, () => {
    console.log('App is listening on port 3001');
});
